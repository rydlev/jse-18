package ru.t1.rydlev.tm.command.task;

import ru.t1.rydlev.tm.api.service.IProjectTaskService;
import ru.t1.rydlev.tm.api.service.ITaskService;
import ru.t1.rydlev.tm.command.AbstractCommand;
import ru.t1.rydlev.tm.enumerated.Role;
import ru.t1.rydlev.tm.enumerated.Status;
import ru.t1.rydlev.tm.model.Task;
import ru.t1.rydlev.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            final String id = task.getId();
            final String name = task.getName();
            final String description = task.getDescription();
            final String status = Status.toName(task.getStatus());
            final String created = DateUtil.formatDate(task.getCreated());
            System.out.printf("%s. %s : %s : %s : %s; id: %s \n", index, name, status, created, description, id);
            index++;
        }
    }

}
